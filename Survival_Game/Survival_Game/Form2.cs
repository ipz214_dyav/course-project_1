﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary_ForGame;

namespace Survival_Game
{
    public partial class Form2 : Form
    {
        public Player player;

        public Enemy enemy0;
        public Enemy enemy1;
        public Enemy enemy2;
        public Enemy enemy3;
        public Enemy enemy4;
        public Enemy enemy5;
        public Enemy enemy6;
        public Enemy enemy7;
        public Enemy enemy8;
        public Enemy enemy9;
        public Enemy enemy10;


        Random r = new Random();

        public int s = 0, m = 0, Demedg;
        public long score = 0;
        public int posX_Update0 = 1, posY_Update0 = 1,
           posX_Update1 = -1, posY_Update1 = -1, posX_Update2 = 0, posY_Update2 = 1,
           posX_Update3 = 1, posY_Update3 = 0, posX_Update4 = 1, posY_Update4 = -1,
           posX_Update5 = -1, posY_Update5 = 1, posX_Update6 = 0, posY_Update6 = -1,
           posX_Update7 = -1, posY_Update7 = 0, posX_Update8 = 0, posY_Update8 = 1,
           posX_Update9 = 1, posY_Update9 = 1, posX_Update10 = -1, posY_Update10 = 0;

        public Form2()
        {
            InitializeComponent();
            player = new Player(Form1.ActiveForm.Width - 280, Form1.ActiveForm.Height /2, GameElement.image_Person,GameElement.hp_pleyer);
            Init();
        }

        public void Init()
        {
            enemy0 = new Enemy(50, 100, Resource1.Enemy);
            enemy1 = new Enemy(450, 100, Resource1.Enemy);
            enemy2 = new Enemy(700, 100, Resource1.Enemy2);
            enemy3 = new Enemy(1200, 100, Resource1.Enemy4_2);
            enemy4 = new Enemy(50, 300, Resource1.Enemy);
            enemy5 = new Enemy(1400, 600, Resource1.Enemy2);
            enemy6 = new Enemy(150, 800, Resource1.Enemy);
            enemy7 = new Enemy(500, 800, Resource1.Enemy);
            enemy8 = new Enemy(1200, 750, Resource1.Enemy4_2);
            enemy9 = new Enemy(1200, 400, Resource1.Enemy);
            enemy10 = new Enemy(1000, 200, Resource1.Enemy4_2);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            BackgroundImage = GameElement.map;

            timer1.Enabled = true;
            timer1.Start();

            timer_Tim.Enabled = true;
            timer_Tim.Start();

            timer2.Enabled = true;
            timer2.Start();

            label_pause.Visible = false;
            label_enter.Visible = false;

            Label_Person.Text = GameElement.Person;
            Label_Name.Text = GameElement.Name;

            Difficulti_LvL();
            Win_or_Lose.Visible = false;

            enemy5.flip = -1;
            label_HP.Text = player.HP.ToString() + " hp";
        }
        public void Finish_Game()
        {
            
            if (m == GameElement.Finish)
            {
                timer1.Stop();
                timer_Tim.Stop();
                timer2.Stop();
                Win_or_Lose.BackColor = Color.Green;
                Win_or_Lose.Visible = true;
                Win_or_Lose.Text = "You Win!";
                Form2.ActiveForm.BackgroundImage = null;
                player.images = Resource1.BlackFon;
                enemy0.images = Resource1.BlackFon;
                enemy1.images = Resource1.BlackFon;
                enemy2.images = Resource1.BlackFon;
                enemy3.images = Resource1.BlackFon;
                enemy4.images = Resource1.BlackFon;
                enemy5.images = Resource1.BlackFon;
                enemy6.images = Resource1.BlackFon;
                enemy7.images = Resource1.BlackFon;
                enemy8.images = Resource1.BlackFon;
                enemy9.images = Resource1.BlackFon;
                enemy10.images = Resource1.BlackFon;

            }
            else if (player.HP <= 0)
            {
                label_HP.Text = "0 hp";
                timer1.Stop();
                timer_Tim.Stop();
                timer2.Stop();
                Win_or_Lose.BackColor = Color.Red;
                Win_or_Lose.Visible = true;
                Win_or_Lose.Text = "You Died!";
                Form2.ActiveForm.BackgroundImage = null;
                player.images = Resource1.BlackFon;
                enemy0.images = Resource1.BlackFon;
                enemy1.images = Resource1.BlackFon;
                enemy2.images = Resource1.BlackFon;
                enemy3.images = Resource1.BlackFon;
                enemy4.images = Resource1.BlackFon;
                enemy5.images = Resource1.BlackFon;
                enemy6.images = Resource1.BlackFon;
                enemy7.images = Resource1.BlackFon;
                enemy8.images = Resource1.BlackFon;
                enemy9.images = Resource1.BlackFon;
                enemy10.images = Resource1.BlackFon;
            }
        }

        private void Form2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Escape)
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer_Tim.Stop();
                label_pause.Visible = true;
                label_enter.Visible = true;
            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                timer1.Start();
                timer2.Start();
                timer3.Start();
                timer_Tim.Start();
                label_pause.Visible = false;
                label_enter.Visible = false;
            }
        }

        public void MoveEn()
        {
            enemy0.posX += posX_Update0;
            enemy0.posY += posY_Update0;

            enemy1.posX += posX_Update1;
            enemy1.posY += posY_Update1;

            enemy2.posX += posX_Update2;
            enemy2.posY += posY_Update2;

            enemy3.posX += posX_Update3;
            enemy3.posY += posY_Update3;

            enemy4.posX += posX_Update4;
            enemy4.posY += posY_Update4;

            enemy5.posX += posX_Update5;
            enemy5.posY += posY_Update5;

            enemy6.posX += posX_Update6;
            enemy6.posY += posY_Update6;

            enemy7.posX += posX_Update7;
            enemy7.posY += posY_Update7;

            enemy8.posX += posX_Update8;
            enemy8.posY += posY_Update8;

            enemy9.posX += posX_Update9;
            enemy9.posY += posY_Update9;

            enemy10.posX += posX_Update10;
            enemy10.posY += posY_Update10;

        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                    player.Move(0, -Speed_Y);
                    break;

                case Keys.A:
                    player.Move(-Speed_X, 0);
                    player.flip = -1;
                    break;

                case Keys.S:
                    player.Move(0, Speed_Y);
                    break;

                case Keys.D:
                    player.Move(Speed_X, 0);
                    player.flip = 1;
                    break;
                //--------------------------------------------
                case Keys.E:
                    player.Move(Speed_DEl_X, -Speed_DEL_Y);
                    player.flip = 1;
                    break;
                case Keys.Q:
                    player.Move(-Speed_DEl_X, -Speed_DEL_Y);
                    player.flip = -1;
                    break;
                case Keys.X:
                    player.Move(Speed_DEl_X, Speed_DEL_Y);
                    player.flip = 1;
                    break;
                case Keys.Z:
                    player.Move(-Speed_DEl_X, Speed_DEL_Y);
                    player.flip = -1;
                    break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Invalidate();
            MapControl_Player();
            MapControl_Enemy();
            Finish_Game();
            MoveEn();
        }
        public bool Taouch(Player player,Enemy enemy)
        {
           PointF delta = new PointF();
            delta.X = (player.posX + player.SizeW / 2) - (enemy.posX + enemy.SizeW / 2);
            delta.Y = (player.posY + player.SizeH / 2) - (enemy.posY + enemy.SizeH / 2);
            if (Math.Abs(delta.X) <= player.SizeW / 2 + enemy.SizeW / 2) 
            {
                if (Math.Abs(delta.Y) <= player.SizeH / 2  + enemy.SizeH / 2)
                {
                    return true;
                }
            }
            return false;
        }
        public void Minus_HP()
        {
            player.HP -= Demedg;
            label_HP.Text = player.HP.ToString() + " hp";

             if (player.HP <= 70)
            {
                label_HP.BackColor = Color.Yellow;
                label_HP.ForeColor = Color.Black;
            }
            if(player.HP <= 40)
            {
                label_HP.BackColor= Color.Red;
                label_HP.ForeColor= Color.White;
            }
        }

        public void MapControl_Player()
        {
            if (player.posX <= 10)
            {
                while (player.posX != 11)
                {
                    player.posX++;
                }
            }
            else if(player.posX >= Form2.ActiveForm.Width-10)
            {
                while (player.posX != Form2.ActiveForm.Width - 11)
                {
                    player.posX--;
                }
            }
            if(player.posY <= 55)
            {
                while(player.posY != 56)
                    player.posY++;
            }
            if(player.posY >= Form2.ActiveForm.Height - 57)
            {
                while(player.posY >= Form2.ActiveForm.Height-58)
                    player.posY --;
            }
        }
        public void MapControl_Enemy()
        {
            if (enemy0.posX <= 10)
            {
                posX_Update0 = Speed_En;
                enemy0.flip = -1;
            }

            else if (enemy0.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update0 = -Speed_En;
                enemy0.flip = 1;
            }

            if (posX_Update0 == 0)
                posX_Update0 = Speed_En;

            if (enemy0.posY <= 55)
                posY_Update0 = Speed_En;

            else if (enemy0.posY >= Form2.ActiveForm.Height - 57)
                posY_Update0 = -Speed_En;

            if (posY_Update0 == 0)
                posY_Update0 = Speed_En;

            if (posY_Update0 == -1 && posX_Update0 == -1)
                enemy0.flip = 1;
            else if (posY_Update0 == 1 && posX_Update0 == 1)
                enemy0.flip = -1;

            //--------------------------------------------------

            if (enemy1.posX <= 10)
            {
                posX_Update1 = Speed_En;
                enemy1.flip = -1;
            }

            else if (enemy1.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update1 = -Speed_En;
                enemy1.flip = 1;
            }

            if (posX_Update1 == 0)
                posX_Update1 = Speed_En;

            if (enemy1.posY <= 55)
                posY_Update1 = Speed_En;

            else if (enemy1.posY >= Form2.ActiveForm.Height - 57)
                posY_Update1 = -Speed_En;

            if (posY_Update1 == 0)
                posY_Update1 = Speed_En;

            if (posY_Update1 == -1 && posX_Update1 == -1)
                enemy1.flip = 1;
            else if (posY_Update1 == 1 && posX_Update1 == 1)
                enemy1.flip = -1;

            //----------------------------------------------------
            if (enemy2.posX <= 10)
            {
                posX_Update2 = Speed_En;
                enemy1.flip = -1;
                enemy2.flip = 1;
            }

            else if (enemy2.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update2 = -Speed_En;
                enemy2.flip = -1;
            }

            if (posX_Update2 == 0)
                posX_Update2 = Speed_En;

            if (enemy2.posY <= 55)
                posY_Update2 = Speed_En;

            else if (enemy2.posY >= Form2.ActiveForm.Height - 57)
                posY_Update2 = -Speed_En;

            if (posY_Update2 == 0)
                posY_Update2 = Speed_En;


            //--------------------------------------------------------
            if (enemy3.posX <= 10)
            {
                posX_Update3 = Speed_En;
                enemy3.flip = -1;
            }

            else if (enemy3.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update3 = -Speed_En;
                enemy3.flip = 1;
            }

            if (posX_Update3 == 0)
                posX_Update3 = Speed_En;

            if (enemy3.posY <= 55)
                posY_Update3 = Speed_En;

            else if (enemy3.posY >= Form2.ActiveForm.Height - 57)
                posY_Update3 = -Speed_En;

            if (posY_Update3 == 0)
                posY_Update3 = Speed_En;

            if (posY_Update3 == -1 && posX_Update3 == -1)
                enemy3.flip = 1;
            else if (posY_Update3 == 1 && posX_Update3 == 1)
                enemy3.flip = -1;

            //--------------------------------------------------------

            if (enemy4.posX <= 10)
            {
                posX_Update4 = Speed_En;
                enemy4.flip = -1;
            }

            else if (enemy4.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update4 = -Speed_En;
                enemy4.flip = 1;
            }

            if (posX_Update4 == 0)
                posX_Update4 = Speed_En;

            if (enemy4.posY <= 55)
                posY_Update4 = Speed_En;

            else if (enemy4.posY >= Form2.ActiveForm.Height - 57)
                posY_Update4 = -Speed_En;

            if (posY_Update4 == 0)
                posY_Update4 = Speed_En;

            if (posY_Update4 == -1 && posX_Update4 == -1)
                enemy4.flip = 1;
            else if (posY_Update4 == 1 && posX_Update4 == 1)
                enemy4.flip = -1;

            //---------------------------------------------------------
            if (enemy5.posX <= 10)
            {
                posX_Update5 = Speed_En;
                enemy5.flip = 1;

            }

            else if (enemy5.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update5 = -Speed_En;
                enemy5.flip = -1; 
            }

            if (posX_Update5 == 0)
                posX_Update5 = Speed_En;

            if (enemy5.posY <= 55)
                posY_Update5 = Speed_En;

            else if (enemy5.posY >= Form2.ActiveForm.Height - 57)
                posY_Update5 = -Speed_En;

            if (posY_Update5 == 0)
                posY_Update5 = Speed_En;

            if (posY_Update5 == -1 && posX_Update5 == -1)
                enemy5.flip = -1;
            else if (posY_Update5 == 1 && posX_Update5 == 1)
                enemy5.flip = 1;
            //-----------------------------------------------------
            if (enemy6.posX <= 10)
            {
                posX_Update6 = Speed_En;
                enemy6.flip = -1;
            }

            else if (enemy6.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update6 = -Speed_En;
                enemy6.flip = 1;
            }

            if (posX_Update6 == 0)
                posX_Update6 = Speed_En;

            if (enemy6.posY <= 55)
                posY_Update6 = Speed_En;

            else if (enemy6.posY >= Form2.ActiveForm.Height - 57)
                posY_Update6 = -Speed_En;

            if (posY_Update6 == 0)
                posY_Update6 = Speed_En;

            if (posY_Update6 == -1 && posX_Update6 == -1)
                enemy6.flip = 1;
            else if (posY_Update6 == 1 && posX_Update6 == 1)
                enemy6.flip = -1;
            //---------------------------------------------------------
            if (enemy7.posX <= 10)
            {
                posX_Update7 = Speed_En;
                enemy7.flip = -1;
            }

            else if (enemy7.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update7 = -Speed_En;
                enemy7.flip = 1;
            }

            if (posX_Update7 == 0)
                posX_Update7 = Speed_En;

            if (enemy7.posY <= 55)
                posY_Update7 = Speed_En;

            else if (enemy7.posY >= Form2.ActiveForm.Height - 57)
                posY_Update7 = -Speed_En;

            if (posY_Update7 == 0)
                posY_Update7 = Speed_En;

            if (posY_Update7 == -1 && posX_Update7 == -1)
                enemy7.flip = 1;
            else if (posY_Update7 == 1 && posX_Update7 == 1)
                enemy7.flip = -1;
            //----------------------------------------------------
            if (enemy8.posX <= 10)
            {
                posX_Update8 = Speed_En;
                enemy8.flip = -1;
            }

            else if (enemy8.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update8 = -Speed_En;
                enemy8.flip = 1;
            }

            if (posX_Update8 == 0)
                posX_Update8 = Speed_En;

            if (enemy8.posY <= 55)
                posY_Update8 = 1;

            else if (enemy8.posY >= Form2.ActiveForm.Height - 57)
                posY_Update8 = -Speed_En;

            if (posY_Update8 == 0)
                posY_Update8 = Speed_En;

            if (posY_Update8 == -1 && posX_Update8 == -1)
                enemy8.flip = 1;
            else if (posY_Update8 == 1 && posX_Update8 == 1)
                enemy8.flip = -1;
            //--------------------------------------------------
            if (enemy9.posX <= 10)
            {
                posX_Update9 = Speed_En;
                enemy9.flip = -1;
            }

            else if (enemy9.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update9 = -Speed_En;
                enemy9.flip = 1;
            }

            if (posX_Update9 == 0)
                posX_Update9 = Speed_En;

            if (enemy9.posY <= 55)
                posY_Update9 = 1;

            else if (enemy9.posY >= Form2.ActiveForm.Height - 57)
                posY_Update9 = -Speed_En;

            if (posY_Update9 == 0)
                posY_Update9 = Speed_En;

            if (posY_Update9 == -1 && posX_Update9 == -1)
                enemy9.flip = 1;
            else if (posY_Update9 == 1 && posX_Update9 == 1)
                enemy9.flip = -1;
            //--------------------------------------------------------
            if (enemy10.posX <= 10)
            {
                posX_Update10 = Speed_En;
                enemy10.flip = -1;
            }

            else if (enemy10.posX >= Form2.ActiveForm.Width - 10)
            {
                posX_Update10 = -Speed_En;
                enemy10.flip = 1;
            }

            if (posX_Update10 == 0)
                posX_Update10 = Speed_En;

            if (enemy10.posY <= 55)
                posY_Update10 = 1;

            else if (enemy10.posY >= Form2.ActiveForm.Height - 57)
                posY_Update10 = -Speed_En;

            if (posY_Update10 == 0)
                posY_Update10 = Speed_En;

            if (posY_Update10 == -1 && posX_Update10 == -1)
                enemy10.flip = 1;
            else if (posY_Update10 == 1 && posX_Update10 == 1)
                enemy10.flip = -1;
        }
        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            player.PlayAnimation_Player(g);

            enemy0.PlayAnimation_Enemy(g);
            enemy1.PlayAnimation_Enemy(g);
            enemy2.PlayAnimation_Enemy(g);
            enemy3.PlayAnimation_Enemy(g);
            enemy4.PlayAnimation_Enemy(g);
            enemy5.PlayAnimation_Enemy(g);
            enemy6.PlayAnimation_Enemy(g);
            enemy7.PlayAnimation_Enemy(g);
            enemy8.PlayAnimation_Enemy(g);
            enemy9.PlayAnimation_Enemy(g);
            enemy10.PlayAnimation_Enemy(g);
        }

        public int Speed_X = GameElement.fast;
        public int Speed_Y = GameElement.fast;

        public int Speed_DEl_X = GameElement.fast--;
        public int Speed_DEL_Y = GameElement.fast--;

        public int Speed_En;

        public void Difficulti_LvL()
        {
            if(GameElement.Dif_LvL == 1)
            {
                Speed_En = 3;
                Demedg = 2;
            }
            else if(GameElement.Dif_LvL == 2)
            {
                Speed_En = 4;
                Demedg = 3;
            }
            else if(GameElement.Dif_LvL == 3)
            {
                Speed_En = 4;
                Demedg = 4;
            }
        }

        private void timer_Tim_Tick(object sender, EventArgs e)
        {
            s++;
            Label_Timer.Text = m.ToString() + ":" + s.ToString();
            Timer_UpDate();
        }

        private void Win_or_Lose_MouseClick(object sender, MouseEventArgs e)
        {
            Form2.ActiveForm.Close();
        }

        public void Timer_UpDate()
        {
            if(s == 60)
            {
                m++;s= 0;
            }
        }   
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (Taouch(player, enemy0))
                Minus_HP();
            if (Taouch(player, enemy1))
                Minus_HP();
            if (Taouch(player, enemy2))
                Minus_HP();
            if (Taouch(player, enemy3))
                Minus_HP();
            if (Taouch(player, enemy4))
                Minus_HP();
            if (Taouch(player, enemy5))
                Minus_HP();
            if (Taouch(player, enemy6))
                Minus_HP();
            if (Taouch(player, enemy7))
                Minus_HP();
            if (Taouch(player, enemy8))
                Minus_HP();
            if (Taouch(player, enemy9))
                Minus_HP();
            if (Taouch(player, enemy10))
                Minus_HP();

        }
        private void timer3_Tick(object sender, EventArgs e)
        {
            score += r.Next(1, 3);
            if (m == 10)
                score *= 2;
            else if (m == 20)
                score *= 3;
            else if (m == 30)
                score *= 5;
            Label_Score.Text = score.ToString();
        } 
        
    }     
}
