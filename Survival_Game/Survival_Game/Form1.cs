﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary_ForGame;

namespace Survival_Game
{
    public partial class Form1 : Form
    {
        Form2 GameForm;
        Form3 Rule;
                       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox_PersonMenu.Visible = false;
            pictureBox_Dif_lvl_Menu.Visible = false;
            comboBox_Dif_lvl.SelectedIndex = 0;
            radioButton_FireMage.Checked = true;
            Label_Error.Visible = false;
            label_map2.Visible = false;
            GameElement.map = Resource1.small_map;
        }
        private void button_start_Click(object sender, EventArgs e)
        {
            if(textBox_Name.Text == "")
            {
                Label_Error.Visible = true;
                return;
            }
            Label_Error.Visible = false;
            GameElement.Name = textBox_Name.Text;

            GameForm = new Form2();
            GameForm.ShowDialog(this);
        }

        private void radioButton_FireMage_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_FireMage.Checked)
            {
                pictureBox_PersonMenu.Visible = true;
                pictureBox_PersonMenu.Image = Resource1.Fire_Mage;
                GameElement.image_Person = Resource1.Fire_Mage;
                GameElement.Person = "FireMage";
                GameElement.fast = 4;
                GameElement.hp_pleyer = 100;
            }
        }

        private void radioButton_Priest_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Priest.Checked) 
            {
                pictureBox_PersonMenu.Visible = true;
                pictureBox_PersonMenu.Image = Resource1.The_Priest;
                GameElement.image_Person = Resource1.The_Priest;
                GameElement.Person = "The Priest";
                GameElement.fast = 3;
                GameElement.hp_pleyer = 150;
            }
        }

        private void radioButton_Thief_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Thief.Checked) 
            {
               pictureBox_PersonMenu.Visible = true;
               pictureBox_PersonMenu.Image= Resource1.Thief;
                GameElement.image_Person = Resource1.Thief;
                GameElement.Person = "Thief";
                GameElement.fast = 5;
                GameElement.hp_pleyer = 85;
            }
        }

        private void comboBox_Dif_lvl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox_Dif_lvl.SelectedIndex == 0)
            {
                pictureBox_Dif_lvl_Menu.Visible = true;
                pictureBox_Dif_lvl_Menu.Image = Resource1.Easily_lvl;
                GameElement.Dif_LvL = 1;
                GameElement.Finish = 15;
            }
            if(comboBox_Dif_lvl.SelectedIndex == 1)
            {
                pictureBox_Dif_lvl_Menu.Visible= true;
                pictureBox_Dif_lvl_Menu.Image= Resource1.Medium_lvl;
                GameElement.Dif_LvL = 2;
                GameElement.Finish = 20;
            }
            if(comboBox_Dif_lvl.SelectedIndex == 2)
            {
                pictureBox_Dif_lvl_Menu.Visible = true;
                pictureBox_Dif_lvl_Menu.Image = Resource1.Hard;
                GameElement.Dif_LvL = 3;
                GameElement.Finish = 30;
            }
        }

        private void button_rule_Click(object sender, EventArgs e)
        {
            Rule = new Form3();
            Rule.ShowDialog(this);
        }
        private void Label_Error_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_map1_Click(object sender, EventArgs e)
        {
            GameElement.map = Resource1.small_map;
            label_map1.Visible = true;
            label_map2.Visible = false;
        }

        private void pictureBox_map2_Click(object sender, EventArgs e)
        {
            GameElement.map = Resource1.small_map2_2;
            label_map2.Visible = true;
            label_map1.Visible = false;
        }
    }
}
