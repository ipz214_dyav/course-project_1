﻿namespace Survival_Game
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBox_Dif_lvl = new System.Windows.Forms.ComboBox();
            this.label_DIf_LvL = new System.Windows.Forms.Label();
            this.label_pers = new System.Windows.Forms.Label();
            this.radioButton_Thief = new System.Windows.Forms.RadioButton();
            this.radioButton_Priest = new System.Windows.Forms.RadioButton();
            this.radioButton_FireMage = new System.Windows.Forms.RadioButton();
            this.label_Name = new System.Windows.Forms.Label();
            this.label_Menu = new System.Windows.Forms.Label();
            this.label_tere = new System.Windows.Forms.Label();
            this.label_SIYC = new System.Windows.Forms.Label();
            this.button_start = new System.Windows.Forms.Button();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.Label_Error = new System.Windows.Forms.Label();
            this.button_rule = new System.Windows.Forms.Button();
            this.pictureBox_Dif_lvl_Menu = new System.Windows.Forms.PictureBox();
            this.pictureBox_PersonMenu = new System.Windows.Forms.PictureBox();
            this.pictureBox_beg = new System.Windows.Forms.PictureBox();
            this.pictureBox_map1 = new System.Windows.Forms.PictureBox();
            this.pictureBox_map2 = new System.Windows.Forms.PictureBox();
            this.label_map2 = new System.Windows.Forms.Label();
            this.label_map1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Dif_lvl_Menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_PersonMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_beg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_map1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_map2)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox_Dif_lvl
            // 
            this.comboBox_Dif_lvl.FormattingEnabled = true;
            this.comboBox_Dif_lvl.Items.AddRange(new object[] {
            "Easily",
            "Medium",
            "Hard"});
            this.comboBox_Dif_lvl.Location = new System.Drawing.Point(679, 543);
            this.comboBox_Dif_lvl.Name = "comboBox_Dif_lvl";
            this.comboBox_Dif_lvl.Size = new System.Drawing.Size(157, 24);
            this.comboBox_Dif_lvl.TabIndex = 15;
            this.comboBox_Dif_lvl.SelectedIndexChanged += new System.EventHandler(this.comboBox_Dif_lvl_SelectedIndexChanged);
            // 
            // label_DIf_LvL
            // 
            this.label_DIf_LvL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_DIf_LvL.AutoSize = true;
            this.label_DIf_LvL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_DIf_LvL.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_DIf_LvL.ForeColor = System.Drawing.Color.White;
            this.label_DIf_LvL.Location = new System.Drawing.Point(378, 541);
            this.label_DIf_LvL.Name = "label_DIf_LvL";
            this.label_DIf_LvL.Size = new System.Drawing.Size(264, 26);
            this.label_DIf_LvL.TabIndex = 14;
            this.label_DIf_LvL.Text = "Choose Difficulti LvL:";
            // 
            // label_pers
            // 
            this.label_pers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_pers.AutoSize = true;
            this.label_pers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_pers.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_pers.ForeColor = System.Drawing.Color.White;
            this.label_pers.Location = new System.Drawing.Point(455, 359);
            this.label_pers.Name = "label_pers";
            this.label_pers.Size = new System.Drawing.Size(187, 26);
            this.label_pers.TabIndex = 9;
            this.label_pers.Text = "Choose Person:";
            // 
            // radioButton_Thief
            // 
            this.radioButton_Thief.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton_Thief.AutoSize = true;
            this.radioButton_Thief.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton_Thief.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Thief.ForeColor = System.Drawing.SystemColors.Window;
            this.radioButton_Thief.Location = new System.Drawing.Point(679, 424);
            this.radioButton_Thief.Name = "radioButton_Thief";
            this.radioButton_Thief.Size = new System.Drawing.Size(84, 27);
            this.radioButton_Thief.TabIndex = 12;
            this.radioButton_Thief.TabStop = true;
            this.radioButton_Thief.Text = "Thief";
            this.radioButton_Thief.UseVisualStyleBackColor = false;
            this.radioButton_Thief.CheckedChanged += new System.EventHandler(this.radioButton_Thief_CheckedChanged);
            // 
            // radioButton_Priest
            // 
            this.radioButton_Priest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton_Priest.AutoSize = true;
            this.radioButton_Priest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton_Priest.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Priest.ForeColor = System.Drawing.SystemColors.Window;
            this.radioButton_Priest.Location = new System.Drawing.Point(679, 391);
            this.radioButton_Priest.Name = "radioButton_Priest";
            this.radioButton_Priest.Size = new System.Drawing.Size(133, 27);
            this.radioButton_Priest.TabIndex = 11;
            this.radioButton_Priest.TabStop = true;
            this.radioButton_Priest.Text = "The Priest";
            this.radioButton_Priest.UseVisualStyleBackColor = false;
            this.radioButton_Priest.CheckedChanged += new System.EventHandler(this.radioButton_Priest_CheckedChanged);
            // 
            // radioButton_FireMage
            // 
            this.radioButton_FireMage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton_FireMage.AutoSize = true;
            this.radioButton_FireMage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radioButton_FireMage.Font = new System.Drawing.Font("Lucida Fax", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_FireMage.ForeColor = System.Drawing.SystemColors.Window;
            this.radioButton_FireMage.Location = new System.Drawing.Point(679, 358);
            this.radioButton_FireMage.Name = "radioButton_FireMage";
            this.radioButton_FireMage.Size = new System.Drawing.Size(126, 27);
            this.radioButton_FireMage.TabIndex = 10;
            this.radioButton_FireMage.TabStop = true;
            this.radioButton_FireMage.Text = "Fire Mage";
            this.radioButton_FireMage.UseVisualStyleBackColor = false;
            this.radioButton_FireMage.CheckedChanged += new System.EventHandler(this.radioButton_FireMage_CheckedChanged);
            // 
            // label_Name
            // 
            this.label_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Name.AutoSize = true;
            this.label_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_Name.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Name.ForeColor = System.Drawing.Color.White;
            this.label_Name.Location = new System.Drawing.Point(557, 287);
            this.label_Name.Name = "label_Name";
            this.label_Name.Size = new System.Drawing.Size(85, 26);
            this.label_Name.TabIndex = 6;
            this.label_Name.Text = "Name:";
            // 
            // label_Menu
            // 
            this.label_Menu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Menu.AutoSize = true;
            this.label_Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_Menu.Font = new System.Drawing.Font("Lucida Fax", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Menu.ForeColor = System.Drawing.Color.White;
            this.label_Menu.Location = new System.Drawing.Point(616, 150);
            this.label_Menu.Name = "label_Menu";
            this.label_Menu.Size = new System.Drawing.Size(119, 38);
            this.label_Menu.TabIndex = 4;
            this.label_Menu.Text = "Menu:";
            // 
            // label_tere
            // 
            this.label_tere.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_tere.AutoSize = true;
            this.label_tere.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_tere.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tere.ForeColor = System.Drawing.Color.White;
            this.label_tere.Location = new System.Drawing.Point(-5, 98);
            this.label_tere.Name = "label_tere";
            this.label_tere.Size = new System.Drawing.Size(1349, 38);
            this.label_tere.TabIndex = 5;
            this.label_tere.Text = "__________________________________________________________________________";
            // 
            // label_SIYC
            // 
            this.label_SIYC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_SIYC.AutoSize = true;
            this.label_SIYC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_SIYC.Font = new System.Drawing.Font("Lucida Fax", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_SIYC.ForeColor = System.Drawing.Color.White;
            this.label_SIYC.Location = new System.Drawing.Point(577, 18);
            this.label_SIYC.Name = "label_SIYC";
            this.label_SIYC.Size = new System.Drawing.Size(219, 91);
            this.label_SIYC.TabIndex = 1;
            this.label_SIYC.Text = "SIYC";
            // 
            // button_start
            // 
            this.button_start.Font = new System.Drawing.Font("Liberation Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_start.Location = new System.Drawing.Point(593, 758);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(288, 66);
            this.button_start.TabIndex = 18;
            this.button_start.Text = "Start";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // textBox_Name
            // 
            this.textBox_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox_Name.Font = new System.Drawing.Font("Lucida Fax", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Name.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox_Name.Location = new System.Drawing.Point(679, 289);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(166, 28);
            this.textBox_Name.TabIndex = 19;
            // 
            // Label_Error
            // 
            this.Label_Error.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Error.AutoSize = true;
            this.Label_Error.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Label_Error.Font = new System.Drawing.Font("Lucida Fax", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Error.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Label_Error.Location = new System.Drawing.Point(454, 245);
            this.Label_Error.Name = "Label_Error";
            this.Label_Error.Size = new System.Drawing.Size(514, 32);
            this.Label_Error.TabIndex = 20;
            this.Label_Error.Text = "Error : You Forgot To Enter a Name";
            // 
            // button_rule
            // 
            this.button_rule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_rule.Font = new System.Drawing.Font("Liberation Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_rule.Location = new System.Drawing.Point(1204, 803);
            this.button_rule.Name = "button_rule";
            this.button_rule.Size = new System.Drawing.Size(109, 34);
            this.button_rule.TabIndex = 21;
            this.button_rule.Text = "Rules";
            this.button_rule.UseVisualStyleBackColor = true;
            this.button_rule.Click += new System.EventHandler(this.button_rule_Click);
            // 
            // pictureBox_Dif_lvl_Menu
            // 
            this.pictureBox_Dif_lvl_Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox_Dif_lvl_Menu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox_Dif_lvl_Menu.Location = new System.Drawing.Point(870, 500);
            this.pictureBox_Dif_lvl_Menu.Name = "pictureBox_Dif_lvl_Menu";
            this.pictureBox_Dif_lvl_Menu.Size = new System.Drawing.Size(54, 99);
            this.pictureBox_Dif_lvl_Menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Dif_lvl_Menu.TabIndex = 16;
            this.pictureBox_Dif_lvl_Menu.TabStop = false;
            // 
            // pictureBox_PersonMenu
            // 
            this.pictureBox_PersonMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox_PersonMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox_PersonMenu.Location = new System.Drawing.Point(870, 349);
            this.pictureBox_PersonMenu.Name = "pictureBox_PersonMenu";
            this.pictureBox_PersonMenu.Size = new System.Drawing.Size(54, 99);
            this.pictureBox_PersonMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox_PersonMenu.TabIndex = 13;
            this.pictureBox_PersonMenu.TabStop = false;
            // 
            // pictureBox_beg
            // 
            this.pictureBox_beg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pictureBox_beg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_beg.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_beg.Name = "pictureBox_beg";
            this.pictureBox_beg.Size = new System.Drawing.Size(1325, 849);
            this.pictureBox_beg.TabIndex = 0;
            this.pictureBox_beg.TabStop = false;
            // 
            // pictureBox_map1
            // 
            this.pictureBox_map1.Image = global::Survival_Game.Resource1.small_map;
            this.pictureBox_map1.Location = new System.Drawing.Point(593, 626);
            this.pictureBox_map1.Name = "pictureBox_map1";
            this.pictureBox_map1.Size = new System.Drawing.Size(100, 94);
            this.pictureBox_map1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox_map1.TabIndex = 22;
            this.pictureBox_map1.TabStop = false;
            this.pictureBox_map1.Click += new System.EventHandler(this.pictureBox_map1_Click);
            // 
            // pictureBox_map2
            // 
            this.pictureBox_map2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_map2.Image")));
            this.pictureBox_map2.Location = new System.Drawing.Point(781, 626);
            this.pictureBox_map2.Name = "pictureBox_map2";
            this.pictureBox_map2.Size = new System.Drawing.Size(100, 94);
            this.pictureBox_map2.TabIndex = 23;
            this.pictureBox_map2.TabStop = false;
            this.pictureBox_map2.Click += new System.EventHandler(this.pictureBox_map2_Click);
            // 
            // label_map2
            // 
            this.label_map2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_map2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_map2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_map2.ForeColor = System.Drawing.Color.White;
            this.label_map2.Location = new System.Drawing.Point(778, 712);
            this.label_map2.Name = "label_map2";
            this.label_map2.Size = new System.Drawing.Size(115, 21);
            this.label_map2.TabIndex = 24;
            this.label_map2.Text = "______________";
            // 
            // label_map1
            // 
            this.label_map1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_map1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_map1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_map1.ForeColor = System.Drawing.Color.White;
            this.label_map1.Location = new System.Drawing.Point(590, 712);
            this.label_map1.Name = "label_map1";
            this.label_map1.Size = new System.Drawing.Size(115, 21);
            this.label_map1.TabIndex = 25;
            this.label_map1.Text = "______________";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1325, 849);
            this.Controls.Add(this.label_map1);
            this.Controls.Add(this.label_map2);
            this.Controls.Add(this.pictureBox_map2);
            this.Controls.Add(this.pictureBox_map1);
            this.Controls.Add(this.button_rule);
            this.Controls.Add(this.Label_Error);
            this.Controls.Add(this.textBox_Name);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.pictureBox_Dif_lvl_Menu);
            this.Controls.Add(this.comboBox_Dif_lvl);
            this.Controls.Add(this.label_DIf_LvL);
            this.Controls.Add(this.pictureBox_PersonMenu);
            this.Controls.Add(this.radioButton_Thief);
            this.Controls.Add(this.radioButton_Priest);
            this.Controls.Add(this.radioButton_FireMage);
            this.Controls.Add(this.label_pers);
            this.Controls.Add(this.label_Name);
            this.Controls.Add(this.label_tere);
            this.Controls.Add(this.label_Menu);
            this.Controls.Add(this.label_SIYC);
            this.Controls.Add(this.pictureBox_beg);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(1343, 896);
            this.MinimumSize = new System.Drawing.Size(1343, 896);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SIYC_Menu";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Dif_lvl_Menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_PersonMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_beg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_map1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_map2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox_PersonMenu;
        private System.Windows.Forms.PictureBox pictureBox_Dif_lvl_Menu;
        private System.Windows.Forms.ComboBox comboBox_Dif_lvl;
        private System.Windows.Forms.Label label_DIf_LvL;
        private System.Windows.Forms.Label label_pers;
        private System.Windows.Forms.RadioButton radioButton_Thief;
        private System.Windows.Forms.RadioButton radioButton_Priest;
        private System.Windows.Forms.RadioButton radioButton_FireMage;
        private System.Windows.Forms.Label label_Name;
        private System.Windows.Forms.Label label_Menu;
        private System.Windows.Forms.Label label_tere;
        private System.Windows.Forms.Label label_SIYC;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.PictureBox pictureBox_beg;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label Label_Error;
        private System.Windows.Forms.Button button_rule;
        private System.Windows.Forms.PictureBox pictureBox_map1;
        private System.Windows.Forms.PictureBox pictureBox_map2;
        private System.Windows.Forms.Label label_map2;
        private System.Windows.Forms.Label label_map1;
    }
}

