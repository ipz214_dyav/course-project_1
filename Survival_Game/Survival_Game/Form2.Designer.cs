﻿namespace Survival_Game
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.Label_Score = new System.Windows.Forms.Label();
            this.Label_Timer = new System.Windows.Forms.Label();
            this.timer_Tim = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.Label_Name = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Label_Person = new System.Windows.Forms.Label();
            this.Win_or_Lose = new System.Windows.Forms.Label();
            this.label_HP = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.label_pause = new System.Windows.Forms.Label();
            this.label_enter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label1.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(1148, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Score:";
            // 
            // Label_Score
            // 
            this.Label_Score.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Score.AutoSize = true;
            this.Label_Score.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Label_Score.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Score.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Label_Score.Location = new System.Drawing.Point(1238, 9);
            this.Label_Score.Name = "Label_Score";
            this.Label_Score.Size = new System.Drawing.Size(27, 26);
            this.Label_Score.TabIndex = 1;
            this.Label_Score.Text = "0";
            // 
            // Label_Timer
            // 
            this.Label_Timer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Timer.AutoSize = true;
            this.Label_Timer.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Label_Timer.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label_Timer.Font = new System.Drawing.Font("Lucida Fax", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Timer.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Label_Timer.Location = new System.Drawing.Point(355, 9);
            this.Label_Timer.Name = "Label_Timer";
            this.Label_Timer.Size = new System.Drawing.Size(83, 38);
            this.Label_Timer.TabIndex = 4;
            this.Label_Timer.Text = " 0:0";
            // 
            // timer_Tim
            // 
            this.timer_Tim.Interval = 1000;
            this.timer_Tim.Tick += new System.EventHandler(this.timer_Tim_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label3.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(5, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 26);
            this.label3.TabIndex = 7;
            this.label3.Text = "Name:";
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Label_Name.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Name.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Label_Name.Location = new System.Drawing.Point(98, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(72, 26);
            this.Label_Name.TabIndex = 8;
            this.Label_Name.Text = "none";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label4.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(6, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 26);
            this.label4.TabIndex = 9;
            this.label4.Text = "Person:";
            // 
            // Label_Person
            // 
            this.Label_Person.AutoSize = true;
            this.Label_Person.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Label_Person.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Person.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Label_Person.Location = new System.Drawing.Point(114, 47);
            this.Label_Person.Name = "Label_Person";
            this.Label_Person.Size = new System.Drawing.Size(72, 26);
            this.Label_Person.TabIndex = 10;
            this.Label_Person.Text = "none";
            // 
            // Win_or_Lose
            // 
            this.Win_or_Lose.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Win_or_Lose.BackColor = System.Drawing.Color.Green;
            this.Win_or_Lose.Font = new System.Drawing.Font("Lucida Fax", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Win_or_Lose.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Win_or_Lose.Location = new System.Drawing.Point(506, 216);
            this.Win_or_Lose.Name = "Win_or_Lose";
            this.Win_or_Lose.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Win_or_Lose.Size = new System.Drawing.Size(439, 184);
            this.Win_or_Lose.TabIndex = 11;
            this.Win_or_Lose.Text = "Yor Win!";
            this.Win_or_Lose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Win_or_Lose.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Win_or_Lose_MouseClick);
            // 
            // label_HP
            // 
            this.label_HP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_HP.BackColor = System.Drawing.Color.Green;
            this.label_HP.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_HP.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label_HP.Location = new System.Drawing.Point(776, 40);
            this.label_HP.Name = "label_HP";
            this.label_HP.Size = new System.Drawing.Size(279, 33);
            this.label_HP.TabIndex = 12;
            this.label_HP.Text = "100 hp";
            this.label_HP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer2
            // 
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Enabled = true;
            this.timer3.Interval = 2000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // label_pause
            // 
            this.label_pause.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_pause.BackColor = System.Drawing.Color.Ivory;
            this.label_pause.Font = new System.Drawing.Font("Lucida Fax", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_pause.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_pause.Location = new System.Drawing.Point(462, 196);
            this.label_pause.Name = "label_pause";
            this.label_pause.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_pause.Size = new System.Drawing.Size(499, 224);
            this.label_pause.TabIndex = 13;
            this.label_pause.Text = "Pause";
            this.label_pause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_enter
            // 
            this.label_enter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_enter.AutoSize = true;
            this.label_enter.BackColor = System.Drawing.Color.Ivory;
            this.label_enter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_enter.Font = new System.Drawing.Font("Lucida Fax", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_enter.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_enter.Location = new System.Drawing.Point(862, 574);
            this.label_enter.Name = "label_enter";
            this.label_enter.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_enter.Size = new System.Drawing.Size(303, 28);
            this.label_enter.TabIndex = 14;
            this.label_enter.Text = "Press Enter to Continue";
            this.label_enter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1410, 636);
            this.Controls.Add(this.label_enter);
            this.Controls.Add(this.label_pause);
            this.Controls.Add(this.label_HP);
            this.Controls.Add(this.Win_or_Lose);
            this.Controls.Add(this.Label_Person);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Label_Timer);
            this.Controls.Add(this.Label_Score);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Survival if You Can";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form2_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form2_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form2_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Label_Score;
        private System.Windows.Forms.Label Label_Timer;
        private System.Windows.Forms.Timer timer_Tim;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Label_Person;
        private System.Windows.Forms.Label Win_or_Lose;
        private System.Windows.Forms.Label label_HP;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Label label_pause;
        private System.Windows.Forms.Label label_enter;
    }
}