﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibrary_ForGame
{
    public class Enemy : Set
    {
        public int flip;
        public Image images;

        public Enemy(int posX, int posY, Image images)
        {
            this.posX = posX;
            this.posY = posY;
            this.images = images;
            SizeW = 70;
            SizeH = 70;
            flip = 1;
        }
        public void MoveEN(int dirX, int dirY)
        {
            posX += dirX;
            posY += dirY;
        }
        public void PlayAnimation_Enemy(Graphics g)
        {
            g.DrawImage(images, new Rectangle(new Point(posX - flip * SizeW/2, posY), new Size(flip * SizeW, SizeH)), 0, 0, 100, 100, GraphicsUnit.Pixel);
        }
    }
}
