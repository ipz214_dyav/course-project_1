﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibrary_ForGame
{
    public class Player:Set
    {
        public int flip;
        public Image images;
        public int HP;

        public Player(int posX, int posY, Image images,int HP)
        {
            this.posX = posX;
            this.posY = posY;
            this.images = images;
            SizeW = 40;
            SizeH = 70;
            flip = 1;
            this.HP = HP;
        }
        public void Move(int dirX, int dirY)
        {
            posX += dirX;
            posY += dirY;
        }
        public void PlayAnimation_Player(Graphics g)
        {
            g.DrawImage(images, new Rectangle(new Point(posX - flip * SizeW / 2, posY), new Size(flip * SizeW, SizeH)), 0, 0, 65, 110, GraphicsUnit.Pixel) ;
        }
    }
}
